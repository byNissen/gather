// Determine theme depending on device
var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;
 


// Set Template7 global devices flags
Template7.global = {
    android: isAndroid,
    ios: isIos
};
 
// Define Dom7
var $$ = Dom7;
 
// Change Through navbar layout to Fixed
// if (isAndroid) {
//     // Change class
//     $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
//     // And move Navbar into Page
//     $$('.view .navbar').prependTo('.view .page');
// }
 
// Init App
var myApp = new Framework7({
    // Enable Material theme for Android device only
    //material: isAndroid ? true : false,
    material: true,
    // Enable Template7 pages
    template7Pages: true
});
 
// Init View
var mainView = myApp.addView('.view-main', {
    // Don't worry about that Material doesn't support it
    // F7 will just ignore it for Material theme
    dynamicNavbar: true
});
// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page) {
    // Do something here for "about" page
    myApp.alert("Welcome aboard")
})


//Init storage
var storage = window.localStorage;
var loc = "locDat";
var locObjProto = [];
storage.setItem(loc, JSON.stringify(locObjProto));

// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onSuccess = function(position) {
    /*myApp.alert('Latitude: '          + position.coords.latitude          + '\n' +
    'Longitude: '         + position.coords.longitude         + '\n' +
    'Accuracy: '          + position.coords.accuracy          + '\n' +
    'Heading: '           + position.coords.heading           + '\n' +
    'Speed: '             + position.coords.speed             + '\n' +
    'Timestamp: '         + position.timestamp                + '\n');*/

    var element = document.getElementById('geolocation');
    element.innerHTML = '<p>Latitude: '  + position.coords.latitude     + '</p><br />' +
    '<p>Longitude: ' + position.coords.longitude    + '</p><br />' +
    '<p>Accuracy: '  + position.coords.accuracy     + '</p><br />' +
    '<p>Heading: '   + position.coords.heading      + '</p><br />' +
    '<p>Speed: '     + position.coords.speed        + '</p><br />' +
    '<p>Timestamp: ' + position.timestamp           + '</p><br />' +
    '<hr />'      + element.innerHTML;

    var locObj = JSON.parse(storage.getItem(loc));
    var locCurrent = {"latitude": position.coords.latitude, "longitude": position.coords.longitude, "timestamp": position.timestamp};
    locObj.push(locCurrent);
    storage.setItem(loc, JSON.stringify(locObj));


};

// onError Callback receives a PositionError object
//
function onError(error) {
    myApp.alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

function sendLocToServer() {

    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 

    xmlhttp.open("POST", "http://bynissen.com/api-handler");
    xmlhttp.setRequestHeader("Content-Type", "application/json");

    locObjOld = storage.getItem(loc);
    storage.setItem(loc, JSON.stringify(locObjProto));
    xmlhttp.send(locObjOld);

}

var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { enableHighAccuracy: false, timeout: 3000 });

/*setTimeout(function(){
  sendLocToServer();
  setTimeout(arguments.callee, 10000);
}, 10000);*/